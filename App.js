import React from 'react';
import { createAppContainer, createDrawerNavigator} from 'react-navigation';
import { Image, View, SafeAreaView, ScrollView, StatusBar } from 'react-native';
import { DrawerItems } from 'react-navigation'
import MainScreen from './src/screens/MainScreen';
import SettingsScreen from './src/screens/SettingsScreen';
import styles from './src/components/Base/styles';

export default class App extends React.Component {
    render() {
        return (
            <MainContainer/>
        );
    }
}


const CustomDrawerComponent = (props) => (
    <SafeAreaView>
        <StatusBar hidden={true} />
        <View style={styles.logo}>
            <Image source={require('./assets/clffony-icon.png')} style={{width: 100, height: 100}}/>
        </View>
        <ScrollView>
            <DrawerItems {...props}/>
        </ScrollView>
    </SafeAreaView>
)

const AppDrawerNavigator = createDrawerNavigator({
    Home: {
        screen: MainScreen,
        navigationOptions:{
            drawerLabel: 'Accueil ',
            title: 'Recherche d\'itinéraire',
            header: null,
            drawerIcon: () => (
                <Image
                    source={require('./assets/home.png')}
                    style={styles.icon}
                />
            ),
        }
    },
    Settings: SettingsScreen,
}, {
    contentComponent: CustomDrawerComponent
});

const MainContainer = createAppContainer(AppDrawerNavigator);
