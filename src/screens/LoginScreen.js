import React, {Component} from "react";
import {View, Image, Button, ActivityIndicator, Alert} from "react-native";
import styles from '../components/Base/styles';
import Reinput from 'reinput';
import axios from 'axios';
import {StackActions, NavigationActions} from 'react-navigation';
import {SecureStore} from 'expo';
import { Checkbox } from 'react-native-material-ui'

const REST_API_URL_LOGIN = 'https://clffony.dimitri-berger.fr/rest/login';

const KEY_LOGIN = 'user-login';
const KEY_PASSWORD = 'user-password';
const KEY_IS_STARTUPLOGIN = 'user-startup-login';

// TODO Persister valeur checkbox et les get

class LoginScreen extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isLoading: false,
            error: '',
            isRememberMe: true,
            isStartUpLogin: true
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.navigateToHomeScreenBack = this.navigateToHomeScreenBack.bind(this);
        this.handleInvalidCredentials = this.handleInvalidCredentials.bind(this);
        this.handleCheckBoxRemember = this.handleCheckBoxRemember.bind(this);
        this.handleCheckBoxStartupLogin = this.handleCheckBoxStartupLogin.bind(this);
    }

    navigateToHomeScreenBack(token) {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'Search', params: {userToken: token}})],
        });
        this.props.navigation.dispatch(resetAction);


    }

    componentDidMount() {
        this._getCredentials();
    }

    async _getCredentials() {
        await SecureStore.getItemAsync(KEY_LOGIN).then((data) => {
            this.setState((state) => ({
                ...state,
                email: data
            }));
        }).catch((error) => console.log(error));

        await SecureStore.getItemAsync(KEY_PASSWORD).then((data) => {
            this.setState((state) => ({
                ...state,
                password: data
            }));
        }).catch((error) => console.log(error));

        await SecureStore.getItemAsync(KEY_IS_STARTUPLOGIN).then((data) => {
            this.setState((state) => ({
                ...state,
                isStartUpLogin: data
            }));
        }).catch((error) => console.log(error));

        if(this.props.navigation.state.params && this.props.navigation.state.params.isStartUpLogin){
            this.onSubmit();
        }
    }

    static isStartupLogin(){
        return new Promise(function(resolve, reject) {
            SecureStore.getItemAsync(KEY_IS_STARTUPLOGIN).then((data) => {
                data ? resolve(data) : reject(data);
            }).catch((error) => reject(error));
        })

    }

    static async _setCredentials(email, password, isStartUpLogin) {
        await SecureStore.setItemAsync(KEY_LOGIN, email);
        await SecureStore.setItemAsync(KEY_PASSWORD, password);
        await SecureStore.setItemAsync(KEY_IS_STARTUPLOGIN, JSON.stringify(isStartUpLogin));
    }

    handleInvalidCredentials() {
        this.setState((state) => ({
            ...state,
            isLoading: false,
            error: ' '
        }));

        Alert.alert('Erreur', 'Identifiant ou mot de passe incorrect. ');
    }

    onSubmit() {
        const data = new FormData();
        const {email, password, isRememberMe, isStartUpLogin} = this.state;

        this.setState((state) => ({
            ...state,
            isLoading: true
        }));

        // TODO JSON LOGIN
        data.append('_username', email);
        data.append('_password', password);

        const navigateBack = this.navigateToHomeScreenBack;
        const handleInvalidCredentials = this.handleInvalidCredentials;

        axios.post(REST_API_URL_LOGIN, data)
            .then(function (response) {
                if(!isStartUpLogin && isRememberMe) {
                    LoginScreen._setCredentials(email, password, false);
                } else if(isStartUpLogin){
                    LoginScreen._setCredentials(email, password, true);
                }
                navigateBack(response.data.token);
            })
            .catch(function (error) {
                console.log(error);
                handleInvalidCredentials();
            });
    }

    handleCheckBoxRemember(isChecked){
        this.setState((state) => ({
            ...state,
            isRememberMe: isChecked
        }));
    }

    handleCheckBoxStartupLogin(isChecked){
        this.setState((state) => ({
            ...state,
            isStartUpLogin: isChecked
        }));
    }

    render() {
        let {email, password} = this.state;
        return (
            !this.state.isLoading ? (
                <View style={styles.container}>
                    <View>
                        <Image style={styles.formLogo} source={require('../../assets/clffony-icon.png')}/>
                    </View>
                    <View style={styles.formLogin}>
                        <Reinput error={this.state.error} label='Email' onChangeText={(email) => this.setState({email, error: ''})}
                                 value={email} keyboardType='email-address'/>
                    </View>
                    <View style={styles.formLogin}>
                        <Reinput error={this.state.error} style={styles.formLogin} label='Mot de passe'
                                 onChangeText={(password) => this.setState({password, error: ''})}
                                 value={password} secureTextEntry={true} password={true}/>
                    </View>
                    <View style={styles.formFooter}>
                        <Button
                            onPress={this.onSubmit}
                            title="Valider"
                            color='#00a65a'
                        />
                        <View style={styles.formCheckbox}>
                            <Checkbox label="Se souvenir de moi" value="oui" checked={this.state.isRememberMe} onCheck={this.handleCheckBoxRemember} />
                            <Checkbox label="Me connecter au démarrage" value="oui" checked={this.state.isStartUpLogin} onCheck={this.handleCheckBoxStartupLogin} />
                        </View>
                    </View>

                </View>
            ) : (
                <View style={styles.container}>
                    <ActivityIndicator size="large"/>
                </View>
            )
        )
    }
}

export default LoginScreen;
