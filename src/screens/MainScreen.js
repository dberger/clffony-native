import React, {Component} from "react";
import ResultScreen from "../screens/ResultsScreen";
import {createStackNavigator, createAppContainer} from "react-navigation";
import SearchRoute from "../components/Search/SearchRoute";
import LoginScreen from "./LoginScreen";

class MainScreen extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const isUserLogged = this.props.navigation.state.params ? (this.props.navigation.state.params.userToken) : false;
        if(!isUserLogged) {
            LoginScreen.isStartupLogin().then(() => {
                this.props.navigation.navigate('Login', {isStartUpLogin: true});
            })
        }
    }

    render() {
        const userToken = this.props.navigation.state.params ? this.props.navigation.state.params.userToken : null;
        return (
            <SearchRoute navigation={this.props.navigation} userToken={userToken}/>
        )
    }
}

const
    AppNavigator = createStackNavigator({
        Search: {
            screen: MainScreen
        },
        Results: {
            screen: ResultScreen
        },
        Login: {
            screen: LoginScreen
        }
    });

export default createAppContainer(AppNavigator);

