import React, {Component} from "react";
import {View, ActivityIndicator, Alert} from "react-native";
import styles from '../components/Base/styles';
import axios from "axios";
import RouteItemResult from "../components/Results/RouteItemResult";
import {StackActions, NavigationActions} from "react-navigation";

const REST_API_URL_ROUTES = 'https://clffony.dimitri-berger.fr/rest/get-routes/{stopStart}/{stopEnd}/{nbResults}';

class ResultsScreen extends Component {

    static navigationOptions = {
        title: 'Recherche d\'itinéraire',
    };

    constructor(props) {
        super(props);
        this.state = {
            stopStart: this.props.navigation.state.params.stopStart,
            stopEnd: this.props.navigation.state.params.stopEnd,
            isLoading: true,
            routes: []
        }
    }

    componentDidMount() {
        let url = REST_API_URL_ROUTES.replace("{stopStart}", this.state.stopStart);
        url = url.replace("{stopEnd}", this.state.stopEnd);
        url = url.replace("{nbResults}", '5');
        axios.get(url)
            .then((response) => {
                if(response.data.content !== 'null' && response.data.content !== undefined && response.data.content !== 'undefined' && response.data.content.length > 0) {
                    this.setState((state) => ({
                        ...state,
                        routes: JSON.parse(response.data.content),
                        isLoading: false
                    }));
                } else {
                    Alert.alert('Erreur', 'Aucun résultat n\'a été trouvé.');
                    const navigateAction = StackActions.reset({
                        index: 0,
                        key: null,
                        actions: [NavigationActions.navigate({ routeName: 'Search' })]
                    })
                    this.props.navigation.dispatch(navigateAction)
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return (
            !this.state.isLoading ? (
                <View style={styles.itemContainer}>
                    {this.state.routes.map((item, key) => (
                        <RouteItemResult stopStart={item.stopStart} stopEnd={item.stopEnd} hourStart={item.hourStart} hourEnd={item.hourEnd} countdown={item.countdown} key={key}/>
                    ))}
                </View>
            ) : (
                <View style={styles.container}>
                    <ActivityIndicator size='large'/>
                </View>
            )
        )
    }
}

export default ResultsScreen;
