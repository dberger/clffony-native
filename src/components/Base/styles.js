import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerSearch: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 100
    },
    buttonSubmit: {
        backgroundColor: '#00c0ef'
    },
    buttonLogin: {
        backgroundColor: '#00a65a'
    },
    formLogin: {
        width: 350,
    },
    formCheckbox: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 50
    },
    viewBtn: {
        width: 200,
    },
    formFooter: {
        width: 350,
        height: 100,
    },
    icon: {
        height: 15,
        width: 15
    },
    logo: {
        flex: 1,
        margin: 80,
        alignItems: 'center',
        justifyContent: 'center',
    },
    formLogo: {
        height: 200,
        width: 200,
        marginBottom: 50
    },
    itemContainer: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'column',
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    itemRow: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    textLogin: {
        color : '#00c0ef',
        textAlign: 'center',
        margin: 50
    }
})


export default styles;
