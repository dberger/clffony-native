import React, {Component} from "react";
import axios from 'axios'
import {View, Picker, ActivityIndicator} from "react-native";

class SelectStop extends Component {

    constructor(props) {
        super(props);
        this.state = {selectValue: '', stops: [], isLoading: true};
        this.onValueChanged = this.onValueChanged.bind(this);
    }

    onValueChanged(newValue) {
        this.setState((state) => ({
            ...state,
            selectValue: newValue
        }));
        this.props.onSelectChanged(newValue);
    }

    componentWillReceiveProps(props) {
        const {stopStart, stopEnd} = this.props;
        if (props.stopStart !== stopStart) {
            this.setState((state) => ({
                ...state,
                selectValue: props.stopStart
            }));
        }
        if (props.stopEnd !== stopEnd) {
            this.setState((state) => ({
                ...state,
                selectValue: props.stopEnd
            }));
        }
    }

    componentDidMount() {
        axios.get(this.props.dataUrl)
            .then((response) => {
                this.setState((state) => ({
                    ...state,
                    stops: response.data,
                    isLoading: false
                }));
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return (
            !this.state.isLoading ? (
                <View>
                    <Picker
                        selectedValue={this.state.selectValue}
                        style={{height: 150, width: 200}}
                        onValueChange={(itemValue, itemIndex) =>
                            this.onValueChanged(itemValue)
                        }
                    >
                        <Picker.Item label="Choisir un arrêt" value="" key={-1}/>
                        {this.state.stops.map((item, key) => (
                            <Picker.Item label={item.stopName} value={item.idStop} key={key}/>)
                        )}

                    </Picker>

                </View>
            ) : (
                <View>
                    <ActivityIndicator/>
                </View>
            )
        )
    }
}

export default SelectStop;
