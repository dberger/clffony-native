import React, {Component} from "react";
import axios from 'axios'
import {View, Picker, ActivityIndicator} from "react-native";

class SelectFavorite extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectValue: '',
            favorites: [],
            isLoading: true,
            userToken: this.props.userToken
        };
        this.onValueChanged = this.onValueChanged.bind(this);
    }

    onValueChanged(newValue) {
        this.setState((state) => ({
            ...state,
            selectValue: newValue
        }));
        if (newValue !== '' && newValue !== undefined && newValue !== 'undefined') {
            const selectedFavorite = this.state.favorites.find(fav => fav.idFavorite === newValue);

            this.props.onFavoriteChanged(selectedFavorite.favoriteContent.start, selectedFavorite.favoriteContent.stop);
        }
    }

    componentDidMount() {
        axios.get(this.props.dataUrl, {headers: {Authorization: 'Bearer '.concat(this.state.userToken)}})
            .then((response) => {
                this.setState((state) => ({
                    ...state,
                    favorites: JSON.parse(response.data.content),
                    isLoading: false
                }));
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return (
            !this.state.isLoading ? (
                <View>
                    <Picker
                        selectedValue={this.state.selectValue}
                        style={{height: 150, width: 200}}
                        onValueChange={(itemValue, itemIndex) =>
                            this.onValueChanged(itemValue)
                        }
                    >
                        <Picker.Item label="Recherche rapide via vos favoris" value="" key={-1}/>
                        {this.state.favorites.map((item, key) => (
                            <Picker.Item label={item.favoriteName} value={item.idFavorite} key={key}/>)
                        )}

                    </Picker>

                </View>
            ) : (
                <View>
                    <ActivityIndicator/>
                </View>
            )
        )
    }
}

export default SelectFavorite;
