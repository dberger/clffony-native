import React, {Component} from "react";
import {View, Text, Button, Alert} from "react-native";
import styles from '../Base/styles';
import SelectStop from "../Select/SelectStop";
import SelectFavorite from "../Select/SelectFavorite";

const REST_API_URL_STOPS = 'https://clffony.dimitri-berger.fr/rest/stops';
const REST_API_URL_FAVORITE = 'https://clffony.dimitri-berger.fr/rest/get-favorites';

class SearchRoute extends Component {

    constructor(props) {
        super(props);
        this.state = {
            stopStart: '',
            stopEnd: '',
            modalVisible: false,
            userToken: this.props.userToken
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.stopEndChanged = this.stopEndChanged.bind(this);
        this.stopStartChanged = this.stopStartChanged.bind(this);
        this.onFavoriteChanged = this.onFavoriteChanged.bind(this);
        this.onLoginPressed = this.onLoginPressed.bind(this);
    }

    onSubmit() {
        if (this.state.stopStart && this.state.stopEnd) {
            this.props.navigation.navigate('Results', {
                stopStart: this.state.stopStart,
                stopEnd: this.state.stopEnd
            })
        } else {
            Alert.alert('Erreur', 'Veuilez tout d\'abord choisir 2 arrêts avant de débuter une recherche');
        }
    }

    onLoginPressed() {
        this.props.navigation.navigate('Login');
    }

    stopStartChanged(newStop) {
        this.setState((state) => ({
            ...state,
            stopStart: newStop
        }));
    }

    stopEndChanged(newStop) {
        this.setState((state) => ({
            ...state,
            stopEnd: newStop
        }));
    }

    onFavoriteChanged(stopStart, stopEnd) {
        this.setState((state) => ({
            ...state,
            stopStart: stopStart,
            stopEnd: stopEnd
        }));
    }

    render() {
        return (
            <View style={this.state.userToken ? styles.container : styles.containerSearch}>
                <Text>Recherche d'un itinéraire :</Text>
                {this.state.userToken && this.state.userToken.length > 0 &&
                <SelectFavorite
                    dataUrl={REST_API_URL_FAVORITE}
                    userToken={this.state.userToken}
                    onFavoriteChanged={this.onFavoriteChanged}
                />
                }
                <SelectStop onSelectChanged={this.stopStartChanged} dataUrl={REST_API_URL_STOPS}
                            stopStart={this.state.stopStart}/>
                <SelectStop onSelectChanged={this.stopEndChanged} dataUrl={REST_API_URL_STOPS}
                            stopEnd={this.state.stopEnd}/>
                <View style={styles.viewBtn}>
                    <Button
                        onPress={this.onSubmit}
                        title="Rechercher"
                        color='#00c0ef'
                    />
                </View>
                {!this.state.userToken &&
                <View style={styles.viewBtn}>
                    <Text onPress={this.onLoginPressed} style={styles.textLogin}>Se connecter</Text>
                </View>
                }
            </View>
        )
    }
}

export default SearchRoute;

