import React, {Component} from "react";
import { View, Text } from "react-native";
import styles from '../../components/Base/styles';
import TimerCountdown from "react-native-timer-countdown";

class RouteItemResult extends Component {

    constructor(props){
        super(props);
        this.state = {
            stopStart: this.props.stopStart,
            stopEnd: this.props.stopEnd,
            hourStart: this.props.hourStart,
            hourEnd: this.props.hourEnd,
            countdown: this.props.countdown
        }
    }

    render() {
        return (
            <View style={styles.itemContainer}>
                <View style={styles.itemRow}>
                    <View>
                        <Text>{this.state.stopStart}</Text>
                        <Text>{this.state.hourStart}</Text>
                    </View>
                    <View>
                        <Text>{this.state.stopEnd}</Text>
                        <Text>{this.state.hourEnd}</Text>
                    </View>
                </View>
                <View style={styles.logo}>
                    <TimerCountdown
                        initialMilliseconds={this.state.countdown}
                        formatMilliseconds={(milliseconds) => {
                            const remainingSec = Math.round(milliseconds / 1000);
                            const seconds = parseInt((remainingSec % 60).toString(), 10);
                            const minutes = parseInt(((remainingSec / 60) % 60).toString(), 10);
                            const hours = parseInt((remainingSec / 3600).toString(), 10);
                            const s = seconds < 10 ? '0' + seconds : seconds;
                            const m = minutes < 10 ? '0' + minutes : minutes;
                            let h = hours < 10 ? '0' + hours : hours;
                            h = h === '00' ? '' : h + ':';
                            return h + m + ':' + s;
                        }}
                        allowFontScaling={true}
                        style={{ fontSize: 20 }}
                    />
                </View>
            </View>
        )
    }
}

export default RouteItemResult;
